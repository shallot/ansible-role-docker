# Docker Ansible Role

This is an [Ansible](https://docs.ansible.com/ansible/latest/) role for
managing [Docker](https://www.docker.com/) resources.

## Synopsis

Below please find an example how to apply the role in a playbook:

    - import_role:
        name: "docker/host"
      vars:
        docker_host_images:
          - name: "debian:buster"
          - name: "debian:stretch"

The above code requires the role to be available in `roles/docker`.

## Variables

Role variables are documented in the `*/defaults/main.yml` files, i.e.
[host/defaults/main.yml](./host/defaults/main.yml).

### Users

System users whose login names are included in the `docker_host_group_members`
variable are added to the group of users that are allowed to interact with the
Docker daemon, e.g.

    docker_host_group_members:
      gitlab-runner: ["service : gitlab-runner"]
      bob: null
      alice: null

These user login names are used as keys, the value is a list of tasks that need
invocation via `notify`, if any. Both must exist before the role is applied.
